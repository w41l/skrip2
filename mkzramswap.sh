#!/bin/sh

if [ -r /etc/default/zram ]; then
  . /etc/default/zram
else
  # See /sys/block/zramX/comp_algo for available compression algorithm
  ZRAMCOMP=zstd

  # ZRAM device to use
  ZRAMDEV=/dev/zram0
  # Or if you want to use last available zram device
  #ZRAMDEV=$(/sbin/zramctl -f)

  # Set it to fixed value in gigabytes, for example 4GB:
  ZRAMSIZE=4
  # Or calculate by divide total available RAM by DIVNUM. Change it as you wish.
  #DIVNUM=3

  # Use higher priority than on-disk swap, priority is a value between -1 and 32767
  SWAPPRIORITY=100
fi

zramswap_create() {
  if [ -z $ZRAMSIZE ]; then
    if [ -n $DIVNUM ]; then
      ZRAMSIZE=$(echo "`cat /proc/meminfo | grep MemTotal | awk '{print $2}'` / 1024 /1024 / ${DIVNUM}" | bc)
    else
      echo "You must set value for ZRAMSIZE or DIVNUM. Both cannot be empty."
      exit 1
    fi
  fi

  if /sbin/swaplabel ${ZRAMDEV} | grep -q "UUID:"; then
    echo "$ZRAMDEV is already initialized"
  else
    echo "Initializing $ZRAMDEV as swap device"
    /sbin/zramctl -a ${ZRAMCOMP} -s ${ZRAMSIZE}G ${ZRAMDEV}
    /sbin/mkswap -f -L ZRAMSWAP ${ZRAMDEV}
  fi
}
zramswap_start() {
  if cat /proc/swaps | grep -q ${ZRAMDEV}; then
    echo "${ZRAMDEV} is in use as swap device"
    cat /proc/swaps
    exit 1
  else
    /sbin/swapon -v -p ${SWAPPRIORITY} ${ZRAMDEV}
  fi
}
zramswap_stop() {
  /sbin/swapoff ${ZRAMDEV}
}
zramswap_reset() {
  if cat /proc/swaps | grep -q ${ZRAMDEV}; then
    echo "${ZRAMDEV} is in use. To force: $0 stopreset"
    exit 1
  else
    echo "Resetting $ZRAMDEV"
    # Reset via sysfs because zramctl --reset will remove the device instead.
    echo 1 | tee /sys/block/$(basename ${ZRAMDEV})/reset >/dev/null
    /sbin/zramctl ${ZRAMDEV}
  fi
}
zramswap_status() {
  if cat /proc/swaps | grep -q ${ZRAMDEV}; then
    echo "${ZRAMDEV} is in use"
  else
    if /sbin/swaplabel ${ZRAMDEV} 2>/dev/null | grep -q 'UUID:'; then
      echo "$ZRAMDEV is ready to use: $0 start"
    else
      echo "$ZRAMDEV is available: $0 create or start"
    fi
  fi
}

case "$1" in
  start)
    zramswap_create
    zramswap_start
    ;;
  stop)
    zramswap_stop
    ;;
  reset)
    zramswap_reset
    ;;
  stopreset)
    zramswap_stop
    sleep 1
    zramswap_reset
    ;;
  create)
    zramswap_create
    ;;
  status)
    zramswap_status
    ;;
  *)
    echo "$0 start|stop|reset|stopreset|create|status"
esac

