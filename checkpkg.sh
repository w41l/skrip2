#!/bin/sh

if [ $(id -u) -gt 0 ]; then
  echo "Please run this script using root privilege"
  exit 1
fi

echo "Package name/suffixes (ex. glibc or _SBo or _SBo alien _wls)"
echo "############################################################"
echo "#      Please note, sometime missing file results are      #"
echo "#   false-positive. Mostly because encoding naming issue.  #"
echo "#     Check it with 'LANG=C ls /path/to/missing_file*'     #"
echo "#       (Without quote) at your shell after examine        #"
echo "#         missing file results in /tmp/ directory.         #"
echo "############################################################"
echo -n "Input: "
read PKGSUFFIXES

PKGUPGRADE="/tmp/missing-dependency-$(date +%Y%m%dT%H%M%S).txt"
PKGMISSING="/tmp/missing-files-$(date +%Y%m%dT%H%M%S).txt"

PKGLISTTMP="$(mktemp /tmp/checklib-list.XXXXXX)"

ARCH=`uname -m`
if [ "$ARCH" == "x86_64" ]; then
    LIBSUFFIX="64"
else
    LIBSUFFIX=""
fi

unset PKG FILE BINFILE PKGSUFFIX
if [ -z "$PKGSUFFIXES" ]; then
  find /var/log/packages/ ! -name 'devs-*' ! -name 'kernel-*' | sort -u | tee -a $PKGLISTTMP >/dev/null
else
  for PKGSUFFIX in ${PKGSUFFIXES}; do
    find /var/log/packages/ -name "*${PKGSUFFIX}*" ! -name 'devs-*' ! -name 'kernel-*' | sort -u | tee -a $PKGLISTTMP >/dev/null
  done
fi

cat ${PKGLISTTMP} | while read PKG; do
  PKGTESTTMP="$(mktemp /tmp/checklib-test.XXXXXX)"
  PKGNAME="$(basename $PKG)"

  echo -n "Checking $PKGNAME: " | tee -a $PKGTESTTMP
  echo "" >> $PKGTESTTMP
  grep -E '^(\w+\/.+\w)$' $PKG | sed -E '/(install|lib(|64)\/incoming)\//d' | sed -E '/\/.+\.la$/d' | sed -E '/\/.+\.new$/d' | while read FILE; do
    if test -e "/${FILE}"; then
      # test ELF binary only
      scanelf -B /${FILE} | grep 'ET_' | while read -r f1 f2 ; do
        if ldd "$f2" 2>/dev/null | grep -q 'not found'; then
	  echo "--> $f2: FAILED" >> $PKGTESTTMP
	  ldd $f2 2>/dev/null | grep 'not found' | sed 's/^[ \t]/ `-> /g' >>$PKGTESTTMP
        fi
      done
    else
      echo "-> /$FILE: MISSING" >> $PKGTESTTMP
    fi
  done

  if cat "$PKGTESTTMP" | grep -q -E 'FAILED|MISSING'; then
    echo "BAD"
    if grep -q ': FAILED' $PKGTESTTMP 2>/dev/null ; then
      cat $PKGTESTTMP | sed -E '/(MISSING|Checking)/d'
      cat $PKGTESTTMP | sed '/MISSING/d' >>$PKGUPGRADE
      echo "" >>$PKGUPGRADE
    elif grep -q ': MISSING' $PKGTESTTMP 2>/dev/null ; then
      cat $PKGTESTTMP | sed -E '/(FAILED|Checking)/d'
      cat $PKGTESTTMP | sed '/FAILED/d' >> $PKGMISSING
      echo "" >>$PKGMISSING
    fi
  else
    echo "GOOD"
  fi

  rm -f $PKGTESTTMP
done

if [ -s "$PKGUPGRADE" ]; then
  echo "Failed dependency list: $PKGUPGRADE"
fi

if [ -s "$PKGMISSING" ]; then
  echo "Missing files list: $PKGMISSING"
fi

rm -f $PKGLISTTMP
unset PKGSUFFIXES PKGSUFFIX PKG FILE BINFILE PKGLISTTMP PKGUPGRADETMP PKGMISSING PKGUPGRADE
